/**
 * Created on 01/11/2019.
 * @author M. Samil Atesoglu
 */

#include <stdlib.h>
#include <stdio.h>

#include <test/run_tests.h>
#include <test/lib/utils/unit_test_strutils.h>
#include <test/lib/collections/unit_test_list.h>
#include <test/lib/collections/unit_test_queue.h>
#include <test/lib/collections/unit_test_priority_queue.h>
#include <test/lib/memory/unit_test_memutils.h>

int
main(int argc, char *argv[])
{
    bool successful = test_all();
    if (!successful)
    {
        fprintf(stderr, "%-6s Unit tests have failed.\n", "[FAIL]");
        exit(EXIT_FAILURE);
    }
    else
        printf("%-6s All unit tests have passed.\n", "[DONE]");
    return EXIT_SUCCESS;
}

bool
test_all()
{
    printf("%-6s Testing all unit tests...\n", "[INFO]");

    bool ok = true;

    test_linked_list(&ok);
    test_strutils(&ok);
    test_queue(&ok);
    test_priority_queue(&ok);
    test_memutils(&ok);

    return ok;
}