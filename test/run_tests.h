/**
 * Created on 01/11/2019.
 * @author M. Samil Atesoglu
 */

#ifndef TEST_RUN_TESTS_H
#define TEST_RUN_TESTS_H

#include <stdbool.h>

bool
test_all();

#endif // TEST_RUN_TESTS_H
