/**
 * Created on 19/11/2019.
 * @author M. Samil Atesoglu
 */

#ifndef UNIT_TEST_STRUTILS_H
#define UNIT_TEST_STRUTILS_H

#include <stdbool.h>

void
test_strutils(bool *ok);

bool
test_trim_both_sides_spaced(bool *ok);

#endif // UNIT_TEST_STRUTILS_H
