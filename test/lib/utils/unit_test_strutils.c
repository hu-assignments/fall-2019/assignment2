/**
 * Created on 19/11/2019.
 * @author M. Samil Atesoglu
 */

#include <test/lib/utils/unit_test_strutils.h>
#include <lib/utils/strutils.h>
#include <stdio.h>

void
test_strutils(bool *ok)
{
    printf("%-6s Testing strutils...\n", "[TEST]");

    printf("%-6s test_trim_both_sides_spaced\n",
           test_trim_both_sides_spaced(ok) ? "[OK]" : "[FAIL]");
}

bool
test_trim_both_sides_spaced(bool *ok)
{
    char *test = strdup("  sd asd  ");
    char *trimmed = trim(test);

    bool passed = EQUALS(trimmed, "sd asd");
    if (*ok)
        *ok = passed;

    free(test);

    return passed;
}