
/**
 * Created on 30/10/2019.
 * @author M. Samil Atesoglu
 */

#include <stdio.h>
#include <test/lib/collections/unit_test_list.h>
#include <lib/collections/linkedlist.h>
#include <lib/utils/strutils.h>
#include <unistd.h>
#include <lib/utils/fileutils.h>

void
test_linked_list(bool *ok)
{
    printf("%-6s Testing linked_list...\n", "[TEST]");

    printf("%-6s test_list_length_is_five_when_there_are_five_elements\n",
           test_list_length_is_five_when_there_are_five_elements(ok) ? "[OK]" : "[FAIL]");

    printf("%-6s test_list_length_is_one_when_there_is_one_element\n",
           test_list_length_is_one_when_there_is_one_element(ok) ? "[OK]" : "[FAIL]");

    printf("%-6s test_list_length_is_zero_when_empty\n", test_list_length_is_zero_when_empty(ok) ? "[OK]" : "[FAIL]");

    printf("%-6s test_free_list\n", test_free_list(ok) ? "[OK]" : "[FAIL]");

    printf("%-6s test_list_get_index_returns_correct_value\n", test_list_get_index_returns_correct_value(ok) ? "[OK]" : "[FAIL]");

    printf("%-6s test_list_pop_until_one_element_present\n", test_list_pop_until_one_element_present(ok) ? "[OK]" : "[FAIL]");

    printf("%-6s test_list_iterate\n", test_list_iterate(ok) ? "[OK]" : "[FAIL]");

    printf("%-6s test_list_find_item\n", test_list_find_item(ok) ? "[OK]" : "[FAIL]");
}

bool
test_list_get_index_returns_correct_value(bool *ok)
{
    LinkedList string_list;
    LinkedList__init(&string_list, NULL);

    LinkedList__push(&string_list, "1abcdsfadfdfdsf");
    LinkedList__push(&string_list, "2csadade");
    LinkedList__push(&string_list, "3fadsasdsgh");
    LinkedList__push(&string_list, "4ijasdk");
    LinkedList__push(&string_list, "5gkds");

    char *str = LLIST__GET(&string_list, 1, char *);

    bool passed = EQUALS(str, "2csadade");
    if (*ok == true)
        *ok = passed;

    LinkedList__free(&string_list);

    return passed;
}

bool
test_list_length_is_five_when_there_are_five_elements(bool *ok)
{
    LinkedList string_list;
    LinkedList__init(&string_list, NULL);

    LinkedList__push(&string_list, "1abcdsfadfdfdsf");
    LinkedList__push(&string_list, "2csadade");
    LinkedList__push(&string_list, "3fadsasdsgh");
    LinkedList__push(&string_list, "4ijasdk");
    LinkedList__push(&string_list, "5gkds");

    bool passed = LinkedList__length(&string_list) == 5;
    if (*ok == true)
        *ok = passed;

    LinkedList__free(&string_list);

    return passed;
}

bool
test_list_length_is_one_when_there_is_one_element(bool *ok)
{
    LinkedList string_list;
    LinkedList__init(&string_list, NULL);

    LinkedList__push(&string_list, "1abcdsfadfdfdsf");

    bool passed = LinkedList__length(&string_list) == 1;
    if (*ok == true)
        *ok = passed;

    LinkedList__free(&string_list);

    return passed;
}

bool
test_list_length_is_zero_when_empty(bool *ok)
{
    LinkedList string_list;
    LinkedList__init(&string_list, NULL);

    LinkedList__push(&string_list, "1abcdsfadfdfdsf");
    LinkedList__push(&string_list, "2csadade");
    LinkedList__push(&string_list, "3fadsasdsgh");

    LinkedList__pop(&string_list);
    LinkedList__pop(&string_list);
    LinkedList__pop(&string_list);

    bool passed = LinkedList__length(&string_list) == 0;
    if (*ok == true)
        *ok = passed;

    LinkedList__free(&string_list);

    return passed;
}

bool
test_list_pop_until_one_element_present(bool *ok)
{
    LinkedList string_list;
    LinkedList__init(&string_list, NULL);

    LinkedList__push_back(&string_list, "1abcdsfadfdfdsf");
    LinkedList__push_back(&string_list, "2csadade");
    LinkedList__push_back(&string_list, "3fadsasdsgh");

    LinkedList__pop(&string_list);
    LinkedList__pop(&string_list);

    bool passed = EQUALS("3fadsasdsgh", LinkedList__pop(&string_list));
    if (*ok == true)
        *ok = passed;

    LinkedList__free(&string_list);

    return passed;
}

bool
test_free_list(bool *ok)
{
    LinkedList string_list;
    LinkedList__init(&string_list, &free_element);

    LinkedList__push(&string_list, strdup("1first"));
    LinkedList__push(&string_list, strdup("2second"));

    LinkedList__free(&string_list);

    bool passed = string_list.head == NULL;
    if (*ok == true)
        *ok = passed;

    return passed;
}

void
iterator(void *data)
{
    char *str = (char *) data;
    FILE *f = fopen("test.txt", "a");
    fprintf(f, "%s\n", str);
    fclose(f);
}

bool
test_list_iterate(bool *ok)
{
    LinkedList *string_list = LinkedList__create(&free);

    LinkedList__push_back(string_list, strdup("1first"));
    LinkedList__push_back(string_list, strdup("2second"));
    LinkedList__push_back(string_list, strdup("3third"));

    LinkedList__for_each(string_list, &iterator);

    LinkedList__destroy(string_list);

    char *out = nullable_read_file("test.txt");

    bool passed = CONTAINS(out, "1first\n2second\n3third\n");

    free(out);

    if (*ok == true)
        *ok = passed;

    return passed;
}

void
free_element(void *data)
{
    free(data);
}

bool
predicate(void *data, void *value)
{
    char *str = (char *) data;
    if (EQUALS(str, (char *) value))
        return true;
    return false;
}

bool
test_list_find_item(bool *ok)
{
    LinkedList *string_list = LinkedList__create(&free);

    LinkedList__push_back(string_list, strdup("1first"));
    LinkedList__push_back(string_list, strdup("2second"));
    LinkedList__push_back(string_list, strdup("3third"));

    char *elem = LinkedList__get_element_by(string_list, &predicate, "2second");

    bool passed = CONTAINS(elem, "2second");

    if (*ok == true)
        *ok = passed;

    LinkedList__destroy(string_list);

    return passed;
}