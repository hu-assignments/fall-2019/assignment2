/**
 * Created on 19/11/2019.
 * @author M. Samil Atesoglu
 */

#ifndef UNIT_TEST_QUEUE_H
#define UNIT_TEST_QUEUE_H

#include <stdbool.h>

void
test_queue(bool *ok);

bool
test_queue_size_is_five_when_there_are_five_elements(bool *ok);

bool
test_queue_size_is_zero_when_emptied(bool *ok);

bool
test_queue_dequeue(bool *ok);

bool
test_queue_size_is_one_when_dequeued_to_one_element(bool *ok);

#endif // UNIT_TEST_QUEUE_H
