/**
 * Created on 19/11/2019.
 * @author M. Samil Atesoglu
 */

#include <test/lib/collections/unit_test_queue.h>
#include <lib/collections/queue.h>
#include <lib/utils/strutils.h>
#include <stdio.h>

void
test_queue(bool *ok)
{
    printf("%-6s Testing queue...\n", "[TEST]");

    printf("%-6s test_queue_size_is_five_when_there_are_five_elements\n",
           test_queue_size_is_five_when_there_are_five_elements(ok) ? "[OK]" : "[FAIL]");

    printf("%-6s test_queue_size_is_zero_when_emptied\n",
           test_queue_size_is_zero_when_emptied(ok) ? "[OK]" : "[FAIL]");

    printf("%-6s test_queue_dequeue\n",
           test_queue_dequeue(ok) ? "[OK]" : "[FAIL]");

    printf("%-6s test_queue_size_is_one_when_dequeued_to_one_element\n",
           test_queue_size_is_one_when_dequeued_to_one_element(ok) ? "[OK]" : "[FAIL]");
}

bool
test_queue_size_is_five_when_there_are_five_elements(bool *ok)
{
    Queue string_queue;
    Queue__init(&string_queue, NULL);

    Queue__enqueue(&string_queue, "1abcdsfadfdfdsf");
    Queue__enqueue(&string_queue, "2csadade");
    Queue__enqueue(&string_queue, "3fadsasdsgh");
    Queue__enqueue(&string_queue, "4ijasdk");
    Queue__enqueue(&string_queue, "5gkds");

    bool passed = Queue__size(&string_queue) == 5;
    if (*ok == true)
        *ok = passed;

    Queue__free(&string_queue);

    return passed;
}

bool
test_queue_size_is_zero_when_emptied(bool *ok)
{
    Queue string_queue;
    Queue__init(&string_queue, NULL);

    Queue__enqueue(&string_queue, "1abcdsfadfdfdsf");
    Queue__enqueue(&string_queue, "2csadade");
    Queue__enqueue(&string_queue, "3fadsasdsgh");
    Queue__enqueue(&string_queue, "4ijasdk");
    Queue__enqueue(&string_queue, "5gkds");

    Queue__dequeue(&string_queue);
    Queue__dequeue(&string_queue);
    Queue__dequeue(&string_queue);
    Queue__dequeue(&string_queue);
    Queue__dequeue(&string_queue);

    bool passed = Queue__size(&string_queue) == 0;
    if (*ok == true)
        *ok = passed;

    Queue__free(&string_queue);

    return passed;
}

bool
test_queue_size_is_one_when_dequeued_to_one_element(bool *ok)
{
    Queue string_queue;
    Queue__init(&string_queue, NULL);

    Queue__enqueue(&string_queue, "1abcdsfadfdfdsf");
    Queue__enqueue(&string_queue, "2csadade");
    Queue__enqueue(&string_queue, "3fadsasdsgh");
    Queue__enqueue(&string_queue, "4ijasdk");
    Queue__enqueue(&string_queue, "5gkds");

    Queue__dequeue(&string_queue);
    Queue__dequeue(&string_queue);
    Queue__dequeue(&string_queue);
    Queue__dequeue(&string_queue);

    bool passed = Queue__size(&string_queue) == 1 && (EQUALS("5gkds", Queue__dequeue(&string_queue)));
    if (*ok == true)
        *ok = passed;

    Queue__free(&string_queue);

    return passed;
}

bool
test_queue_dequeue(bool *ok)
{
    Queue string_queue;
    Queue__init(&string_queue, NULL);

    Queue__enqueue(&string_queue, "1abcdsfadfdfdsf");
    Queue__enqueue(&string_queue, "2csadade");
    Queue__enqueue(&string_queue, "3fadsasdsgh");
    Queue__enqueue(&string_queue, "4ijasdk");
    Queue__enqueue(&string_queue, "5gkds");

    Queue__dequeue(&string_queue);
    Queue__dequeue(&string_queue);
    Queue__dequeue(&string_queue);
    Queue__dequeue(&string_queue);

    bool passed = EQUALS("5gkds", Queue__dequeue(&string_queue));
    if (*ok == true)
        *ok = passed;

    Queue__free(&string_queue);

    return passed;
}