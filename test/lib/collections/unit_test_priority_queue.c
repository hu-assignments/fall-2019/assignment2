/**
 * Created on 20/11/2019.
 * @author M. Samil Atesoglu
 */

#include <test/lib/collections/unit_test_priority_queue.h>
#include <lib/collections/queue.h>
#include <lib/collections/priority_queue.h>
#include <lib/utils/memutils.h>
#include <lib/utils/strutils.h>
#include <stdio.h>

void
test_priority_queue(bool *ok)
{
    printf("%-6s Testing PriorityQueue...\n", "[TEST]");

    printf("%-6s test_PriorityQueue__enqueue\n",
           test_PriorityQueue__enqueue(ok) ? "[OK]" : "[FAIL]");

    printf("%-6s test_PriorityQueue__dequeue_all\n",
           test_PriorityQueue__dequeue_all(ok) ? "[OK]" : "[FAIL]");

    printf("%-6s test_PriorityQueue__dequeue_until_one_element_present\n",
           test_PriorityQueue__dequeue_until_one_element_present(ok) ? "[OK]" : "[FAIL]");
}

bool
test_PriorityQueue__enqueue(bool *ok)
{
    Queue string_queue;
    Queue__init(&string_queue, NULL);

    PriorityQueue__enqueue(&string_queue, "first", 1);
    PriorityQueue__enqueue(&string_queue, "second", 2);
    PriorityQueue__enqueue(&string_queue, "third", 4);
    PriorityQueue__enqueue(&string_queue, "fourth", 3);

    char *item = PriorityQueue__dequeue(&string_queue);
    bool passed = EQUALS("third", item);

    item = PriorityQueue__dequeue(&string_queue);
    if (passed)
        passed = EQUALS("fourth", item);

    item = PriorityQueue__dequeue(&string_queue);
    if (passed)
        passed = EQUALS("second", item);

    item = PriorityQueue__dequeue(&string_queue);
    if (passed)
        passed = EQUALS("first", item);

    if (*ok == true)
        *ok = passed;

    Queue__free(&string_queue);

    return passed;
}

bool
test_PriorityQueue__dequeue_until_one_element_present(bool *ok)
{
    Queue string_queue;
    Queue__init(&string_queue, NULL);

    bool passed;
    PriorityQueue__enqueue(&string_queue, "first", 1);
    PriorityQueue__enqueue(&string_queue, "second", 2);
    PriorityQueue__enqueue(&string_queue, "third", 3);

    passed = Queue__size(&string_queue) == 3;
    PriorityQueue__dequeue(&string_queue);
    if (passed)
        passed = Queue__size(&string_queue) == 2;
    PriorityQueue__dequeue(&string_queue);
    if (passed)
        passed = Queue__size(&string_queue) == 1;

    if (passed)
        passed = EQUALS("first",  PriorityQueue__dequeue(&string_queue));

    if (*ok == true)
        *ok = passed;

    Queue__free(&string_queue);

    return passed;
}

bool
test_PriorityQueue__dequeue_all(bool *ok)
{
    Queue string_queue;
    Queue__init(&string_queue, NULL);
    bool passed;
    PriorityQueue__enqueue(&string_queue, "first", 1);
    PriorityQueue__enqueue(&string_queue, "second", 2);
    PriorityQueue__enqueue(&string_queue, "third", 3);

    passed = Queue__size(&string_queue) == 3;
    PriorityQueue__dequeue(&string_queue);
    if (passed)
        passed = Queue__size(&string_queue) == 2;
    PriorityQueue__dequeue(&string_queue);
    if (passed)
        passed = Queue__size(&string_queue) == 1;
    PriorityQueue__dequeue(&string_queue);
    if (passed)
        passed = Queue__size(&string_queue) == 0;

    if (*ok == true)
        *ok = passed;

    Queue__free(&string_queue);

    return passed;
}