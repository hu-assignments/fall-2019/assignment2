/**
 * Created on 20/11/2019.
 * @author M. Samil Atesoglu
 */

#ifndef UNIT_TEST_PRIORITY_QUEUE_H
#define UNIT_TEST_PRIORITY_QUEUE_H

#include <stdbool.h>

void
test_priority_queue(bool *ok);

bool
test_PriorityQueue__enqueue(bool *ok);

bool
test_PriorityQueue__dequeue_until_one_element_present(bool *ok);

bool
test_PriorityQueue__dequeue_all(bool *ok);

#endif // UNIT_TEST_PRIORITY_QUEUE_H
