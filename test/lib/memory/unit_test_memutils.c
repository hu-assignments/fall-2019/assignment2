/**
 * Created on 22/11/2019.
 * @author M. Samil Atesoglu
 */

#include <test/lib/memory/unit_test_memutils.h>
#include <stdio.h>
#include <string.h>
#include <lib/utils/memutils.h>

void
test_memutils(bool *ok)
{
    printf("%-6s Testing memutils...\n", "[TEST]");

    printf("%-6s test_safe_free\n", test_safe_free(ok) ? "[OK]" : "[FAIL]");
    printf("%-6s test_safe_free_through_function\n", test_safe_free_through_function(ok) ? "[OK]" : "[FAIL]");
    // printf("%-6s test_safe_free_with_struct_through_function\n", test_safe_free_with_struct_through_function(ok) ? "[OK]" : "[FAIL]");
    // printf("%-6s test_safe_free_with_struct_through_multiple_functions\n",
    //        test_safe_free_with_struct_through_multiple_functions(ok) ? "[OK]" : "[FAIL]");

}

bool
test_safe_free(bool *ok)
{
    char *str = strdup("1234567890");

    SAFE_FREE(str);

    bool passed = str == NULL;

    if (*ok == true)
        *ok = passed;

    return passed;
}

bool
test_safe_free_through_function(bool *ok)
{
    char *str = strdup("1234567890");

    safe_free_through_function((void **) &str);

    bool passed = str == NULL;

    if (*ok == true)
        *ok = passed;

    return passed;
}

bool
test_safe_free_with_struct_through_function(bool *ok)
{
    struct string_t *str1 = ALLOC(1, struct string_t);
    str1->str = strdup("1234567890");
    str1->len = 10;
    struct int_array_t *int_array = ALLOC(1, struct int_array_t);
    int_array->arr = ALLOC(100, int);
    int_array->len = 100;
    str1->arr = int_array;

    string_t__destroy(&str1);

    bool passed = str1 == NULL;
    if (passed)
        passed = int_array == NULL;

    if (*ok == true)
        *ok = passed;

    return passed;
}

void
fun_in_the_middle(struct string_t **str)
{
    string_t__destroy(str);
}

bool
test_safe_free_with_struct_through_multiple_functions(bool *ok)
{
    struct string_t *str1 = ALLOC(1, struct string_t);
    str1->str = strdup("1234567890");
    str1->len = 10;
    struct int_array_t *int_array = ALLOC(1, struct int_array_t);
    int_array->arr = ALLOC(100, int);
    int_array->len = 100;
    str1->arr = int_array;

    fun_in_the_middle(&str1);

    bool passed = str1 == NULL;
    if (passed)
        passed = int_array == NULL;

    if (*ok == true)
        *ok = passed;

    return passed;
}

void
string_t__destroy(struct string_t **str)
{
    SAFE_FREE((*str)->str);
    int_array_t__destroy(&((*str)->arr));
    SAFE_FREE(*str);
}

void
int_array_t__destroy(struct int_array_t **int_array)
{
    SAFE_FREE(*(*int_array)->arr);
    SAFE_FREE(*int_array);
}

void
safe_free_through_function(void **ptr)
{
    SAFE_FREE(*ptr);
}