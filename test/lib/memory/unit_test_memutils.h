/**
 * Created on 22/11/2019.
 * @author M. Samil Atesoglu
 */

#ifndef UNIT_TEST_MEMUTILS_H
#define UNIT_TEST_MEMUTILS_H

#include <stdbool.h>

struct int_array_t
{
    int *arr;
    unsigned int len;
};

struct string_t
{
    char *str;
    unsigned int len;
    struct int_array_t *arr;
};

void
test_memutils(bool *ok);

void
safe_free_through_function(void **ptr);

bool
test_safe_free(bool *ok);

bool
test_safe_free_through_function(bool *ok);

bool
test_safe_free_with_struct_through_function(bool *ok);

bool
test_safe_free_with_struct_through_multiple_functions(bool *ok);

void
fun_in_the_middle(struct string_t **str);

void
string_t__destroy(struct string_t **str);

void
int_array_t__destroy(struct int_array_t **int_array);

#endif //UNIT_TEST_MEMUTILS_H
