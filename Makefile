OBJS	= lib/utils/fileutils.o lib/utils/mathutils.o lib/utils/strutils.o lib/collections/linkedlist.o lib/collections/priority_queue.o lib/collections/queue.o src/core/main.o lib/collections/stack.o src/core/sellticket.o src/airport/flight.o src/airport/airport.o src/airport/passenger.o src/airport/seat.o lib/utils/memutils.o lib/utils/langutils.o
SOURCE	= lib/utils/fileutils.c lib/utils/mathutils.c lib/utils/strutils.c lib/collections/linkedlist.c lib/collections/priority_queue.c lib/collections/queue.c src/core/main.c lib/collections/stack.c src/core/sellticket.c src/airport/flight.c src/airport/airport.c src/airport/passenger.c src/airport/seat.c lib/utils/memutils.c lib/utils/langutils.c
HEADER	= lib/utils/fileutils.h lib/utils/mathutils.h lib/utils/memutils.h lib/utils/strutils.h lib/collections/linkedlist.h lib/collections/priority_queue.h lib/collections/queue.h lib/collections/stack.h src/core/main.h src/core/sellticket.h src/airport/flight.h src/airport/airport.h src/airport/passenger.h src/airport/seat.h lib/utils/langutils.h
INC=src/ ./
INC_PARAMS=$(foreach d, $(INC), -I$d)
OUT	= sellticket
CC	= gcc
LFLAGS	 = -lm

all: sellticket clean_obj_files

.PHONY: sellticket
sellticket: $(OBJS)
	@echo "[INFO] Linking C executable sellticket..."
	@$(CC)  $^ $(LFLAGS) -o $@
	@echo "[OK]   Built C executable sellticket"

%.o: %.c $(HEADER)
	@echo "[INFO] Compiling and assembling $<"
	@$(CC) $(INC_PARAMS) -c -o $@ $< $(LFLAGS)

.PHONY: clean_obj_files
clean_obj_files:
	@echo "[OK]   Cleaning object files..."
	@rm -f $(OBJS)
	@echo "[OK]   Object files are cleaned."

.PHONY: clean
clean:
	@echo "[INFO] Cleaning project..."
	@rm -f $(OBJS) $(OUT)
	@echo "[OK]   Project cleaned."
