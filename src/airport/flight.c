//
// Created by msa on 21/11/2019.
//

#include <string.h>
#include <stdio.h>

#include <airport/flight.h>
#include <airport/airport.h>
#include <airport/seat.h>
#include <airport/passenger.h>

#include <lib/utils/memutils.h>
#include <lib/utils/langutils.h>
#include <lib/collections/priority_queue.h>

static void
print_passenger(void *ptr);

static void
print_waiting_passenger(void *ptr);

Flight *
Flight__create(char *flight_code)
{
    Flight *flight = ALLOC(1, Flight);
    Flight__init(flight, flight_code);
    return flight;
}

void
Flight__init(Flight *flight, char *flight_code)
{
    flight->flight_code = strdup(flight_code);
    flight->ticket_queue = Queue__create(NULL);
    flight->board = ALLOC(1, Board);
    flight->board->empty_business_seat_stack = Stack__create((void (*)(void *)) &Seat__destroy);
    flight->board->empty_economy_seat_stack = Stack__create((void (*)(void *)) &Seat__destroy);
    flight->board->empty_standard_seat_stack = Stack__create((void (*)(void *)) &Seat__destroy);
    flight->board->occupied_business_seat_stack = Stack__create((void (*)(void *)) &Seat__destroy);
    flight->board->occupied_economy_seat_stack = Stack__create((void (*)(void *)) &Seat__destroy);
    flight->board->occupied_standard_seat_stack = Stack__create((void (*)(void *)) &Seat__destroy);
    flight->is_open = true;
}

void
Flight__add_seat(Flight *flight, unsigned int count, enum travel_class class)
{
    Stack *seat_stack;

    switch (class)
    {
        case BUSINESS:
            seat_stack = flight->board->empty_business_seat_stack;
            break;
        case ECONOMY:
            seat_stack = flight->board->empty_economy_seat_stack;
            break;
        case STANDARD:
            seat_stack = flight->board->empty_standard_seat_stack;
            break;
        default:
            exit(EXIT_FAILURE);
    }

    unsigned int i;
    for (i = 0; i < count; i++)
    {
        Seat *seat = Seat__create(NULL, class);
        Stack__push(seat_stack, seat);
    }
}

void
Flight__destroy_board(Flight *flight)
{
    Stack__destroy(flight->board->empty_standard_seat_stack);
    Stack__destroy(flight->board->empty_economy_seat_stack);
    Stack__destroy(flight->board->empty_business_seat_stack);
    Stack__destroy(flight->board->occupied_business_seat_stack);
    Stack__destroy(flight->board->occupied_economy_seat_stack);
    Stack__destroy(flight->board->occupied_standard_seat_stack);
    free(flight->board);
}

void
Flight__free(Flight *flight)
{
    free(flight->flight_code);
    PriorityQueue__destroy(flight->ticket_queue);
    Flight__destroy_board(flight);
}

void
Flight__destroy(Flight *flight)
{
    Flight__free(flight);
    free(flight);
}

unsigned int
Flight__get_number_of_people_in_ticket_queue(Flight *flight, enum travel_class class)
{
    unsigned int n = 0;
    Node *current = flight->ticket_queue->list->head;
    while (current != NULL)
    {
        if (((Passenger *) (((PriorityItem *) current->data)->val))->class == class)
            n++;
        current = current->next;
    }
    return n;
}

unsigned int
Flight__get_number_of_seats(Flight *flight, enum travel_class class)
{
    switch (class)
    {
        case BUSINESS:
            return Stack__size(flight->board->empty_business_seat_stack) + Stack__size(flight->board->occupied_business_seat_stack);
        case ECONOMY:
            return Stack__size(flight->board->empty_economy_seat_stack) + Stack__size(flight->board->occupied_economy_seat_stack);
        case STANDARD:
            return Stack__size(flight->board->empty_standard_seat_stack) + Stack__size(flight->board->occupied_standard_seat_stack);
        default:
            return 0;
    }
}

unsigned int
Flight__get_total_number_of_tickets_sold(Flight *flight)
{
    return Stack__size(flight->board->occupied_business_seat_stack)
        + Stack__size(flight->board->occupied_economy_seat_stack)
        + Stack__size(flight->board->occupied_standard_seat_stack);
}

void
Flight__sell_all(Flight *flight)
{
    unsigned int b = 0, e = 0, s = 0;

    Queue *excess_customer_queue = Queue__create(NULL);

    while (!Queue__is_empty(flight->ticket_queue))
    {
        Passenger *passenger = PriorityQueue__dequeue(flight->ticket_queue);
        Seat *seat;
        switch (passenger->class)
        {
            case BUSINESS:
                seat = Stack__pop(flight->board->empty_business_seat_stack);
                if (NOTNULL(seat))
                {
                    seat->passenger = passenger;
                    passenger->seat = seat;
                    Stack__push(flight->board->occupied_business_seat_stack, seat), b++;
                }
                else
                    Queue__enqueue(excess_customer_queue, passenger);
                break;
            case ECONOMY:
                seat = Stack__pop(flight->board->empty_economy_seat_stack);
                if (NOTNULL(seat))
                {
                    seat->passenger = passenger;
                    passenger->seat = seat;
                    Stack__push(flight->board->occupied_economy_seat_stack, seat), e++;
                }
                else
                    Queue__enqueue(excess_customer_queue, passenger);
                break;
            case STANDARD:
                seat = Stack__pop(flight->board->empty_standard_seat_stack);
                if (NOTNULL(seat))
                {
                    seat->passenger = passenger;
                    passenger->seat = seat;
                    Stack__push(flight->board->occupied_standard_seat_stack, seat), s++;
                }
                else
                    Queue__enqueue(excess_customer_queue, passenger);
                break;
            default:
                break;
        }
    }

    while (!Queue__is_empty(excess_customer_queue))
    {
        Passenger *passenger = Queue__dequeue(excess_customer_queue);
        if (passenger == NULL)
            break;
        Seat *seat = Stack__pop(flight->board->empty_standard_seat_stack);
        if (seat == NULL)
        {
            PriorityQueue__enqueue(flight->ticket_queue, passenger, (int) (passenger->priority * passenger->class));
        }
        else
        {
            seat->passenger = passenger;
            passenger->seat = seat;
            Stack__push(flight->board->occupied_standard_seat_stack, seat);
            s++;
        }
    }

    Queue__destroy(excess_customer_queue);

    printf("sold %s %d %d %d\n", flight->flight_code,
           Stack__size(flight->board->occupied_business_seat_stack),
           Stack__size(flight->board->occupied_economy_seat_stack),
           Stack__size(flight->board->occupied_standard_seat_stack));
}

void
Flight__print_report(Flight *flight)
{
    printf("report %s\n", flight->flight_code);

    unsigned int b = Stack__size(flight->board->occupied_business_seat_stack);
    unsigned int e = Stack__size(flight->board->occupied_economy_seat_stack);
    unsigned int s = Stack__size(flight->board->occupied_standard_seat_stack);

    printf("business %d\n", b);
    LinkedList__for_each_reversed(flight->board->occupied_business_seat_stack->list->head, &print_passenger);

    printf("economy %d\n", e);
    LinkedList__for_each_reversed(flight->board->occupied_economy_seat_stack->list->head, &print_passenger);

    printf("standard %d\n", s);
    LinkedList__for_each_reversed(flight->board->occupied_standard_seat_stack->list->head, &print_passenger);

    printf("end of report %s\n", flight->flight_code);
}

static void
print_passenger(void *ptr)
{
    printf("%s\n", ((Seat *) ptr)->passenger->name);
}

static void
print_waiting_passenger(void *ptr)
{
    printf("waiting %s\n", ((Seat *) ptr)->passenger->name);
}

void
Flight__close(Flight *flight)
{
    if (flight->is_open)
    {
        flight->is_open = false;
        printf("closed %s %d %d\n", flight->flight_code, Flight__get_total_number_of_tickets_sold(flight),
               Queue__size(flight->ticket_queue));
        LinkedList__for_each(flight->ticket_queue->list, &print_waiting_passenger);
    }

}