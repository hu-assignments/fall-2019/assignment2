//
// Created by msa on 21/11/2019.
//

#ifndef ASSIGNMENT2_FLIGHT_H
#define ASSIGNMENT2_FLIGHT_H

#include <lib/collections/stack.h>
#include <lib/collections/queue.h>

enum travel_class;

struct board_t
{
    Stack *empty_business_seat_stack;
    Stack *empty_economy_seat_stack;
    Stack *empty_standard_seat_stack;
    Stack *occupied_business_seat_stack;
    Stack *occupied_economy_seat_stack;
    Stack *occupied_standard_seat_stack;
};

typedef struct board_t Board;

struct flight_t
{
    char *flight_code;
    Queue *ticket_queue;
    Board *board;
    bool is_open;
};

typedef struct flight_t Flight;

Flight *
Flight__create(char *flight_code);

void
Flight__init(Flight *flight, char *flight_code);

void
Flight__free(Flight *flight);

/**
 * Adds seat to the given Flight with specified class.
 *
 * @param flight The Flight that will be modified.
 * @param count The number of seats to be added.
 * @param class The travel class of the new seats.
 */
void
Flight__add_seat(Flight *flight, unsigned int count, enum travel_class class);

/**
 * Frees the dynamically allocated Flight object.
 *
 * @param flight The Flight to be freed.
 */
void
Flight__destroy(Flight *flight);

/**
 * Calculates the number of passengers of given class in the ticket queue.
 *
 * @param flight The Flight to be operated on.
 * @param class The travel class that we want to calculate the number of people in the queue.
 * @return The number of passengers of given class in the ticket queue.
 */
unsigned int
Flight__get_number_of_people_in_ticket_queue(Flight *flight, enum travel_class class);

/**
 *  Returns the number of seats of given travel class of that flight.
 *
 * @param flight The Flight that we want to calculate the seats of.
 * @param class The travel class.
 * @return The number of seats of specified travel class.
 */
unsigned int
Flight__get_number_of_seats(Flight *flight, enum travel_class class);

void
Flight__print_report(Flight *flight);

void
Flight__sell_all(Flight *flight);

/**
 * Closes the flight and makes it read-only, if not already closed.
 *
 * @param flight The Flight that will be closed.
 */
void
Flight__close(Flight *flight);

#endif //ASSIGNMENT2_FLIGHT_H
