//
// Created by msa on 21/11/2019.
//

#include <airport/passenger.h>
#include <lib/utils/memutils.h>
#include <lib/utils/langutils.h>

#include <string.h>

Passenger *
Passenger__create(char *name, Flight *flight, enum travel_class class, enum priority priority)
{
    Passenger *passenger = ALLOC(1, Passenger);
    Passenger__init(passenger, name, flight, class, priority);
    return passenger;
}

void
Passenger__init(Passenger *passenger, char *name, Flight *flight, enum travel_class class, enum priority priority)
{
    passenger->name = strdup(name);
    passenger->flight = flight;
    passenger->class = class;
    passenger->priority = priority;
    passenger->seat = NULL;
}

void
Passenger__free(Passenger *passenger)
{
    if (NOTNULL(passenger))
    {
        free(passenger->name);
        // Seat__destroy(passenger->seat);
        // passenger->seat = NULL;
    }
}

void
Passenger__destroy(Passenger *passenger)
{
    Passenger__free(passenger);
    free(passenger);
}
