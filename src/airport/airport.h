//
// Created by msa on 21/11/2019.
//

#ifndef ASSIGNMENT2_AIRPORT_H
#define ASSIGNMENT2_AIRPORT_H

#include <lib/collections/queue.h>
#include <lib/collections/linkedlist.h>

struct flight_t;
typedef struct flight_t Flight;

struct passenger_t;
typedef struct passenger_t Passenger;

enum travel_class { STANDARD = 2, ECONOMY = 3, BUSINESS = 5 };
enum priority { NONE = 7, VETERAN = 9, DIPLOMAT = 11 };

struct airport_t
{
    LinkedList *flight_list;
    LinkedList *passenger_list;
};

typedef struct airport_t Airport;

/**
 * Initializes the supplied airport and the contents in it.
 *
 * @param airport The Airport to be initialized.
 */
void
Airport__init(Airport *airport);

/**
 * Dynamically allocates an "Airport" object and returns the pointer to it.
 *
 * @return A newly allocated Airport.
 */
Airport *
Airport__create();

void
Airport__free(Airport *airport);

void
Airport__destroy(Airport *airport);

Flight *
Airport__get_flight(Airport *airport, char *name);

void
Airport__schedule_flight(Airport *airport, Flight *flight);

Passenger *
Airport__get_passenger(Airport *airport, char *name);

#endif //ASSIGNMENT2_AIRPORT_H
