/**
 * Created on 21/11/2019.
 * @author M. Samil Atesoglu
 */

#ifndef ASSIGNMENT2_SEAT_H
#define ASSIGNMENT2_SEAT_H

#include <airport/airport.h>
#include <lib/utils/memutils.h>

struct seat_t
{
    enum travel_class class;
    Passenger *passenger;
};

typedef struct seat_t Seat;

Seat *
Seat__create(Passenger *passenger, enum travel_class class);

void
Seat__init(Seat *seat, Passenger *passenger, enum travel_class class);

void
Seat__free(Seat *seat);

void
Seat__destroy(Seat *seat);

#endif //ASSIGNMENT2_SEAT_H
