/**
 * Created on 21/11/2019.
 * @author M. Samil Atesoglu
 */

#include <airport/seat.h>
#include <lib/utils/langutils.h>
#include <airport/passenger.h>

Seat *
Seat__create(Passenger *passenger, enum travel_class class)
{
    Seat *seat = ALLOC(1, Seat);
    Seat__init(seat, passenger, class);
    return seat;
}

void
Seat__init(Seat *seat, Passenger *passenger, enum travel_class class)
{
    seat->passenger = passenger;
    seat->class = class;
}

void
Seat__free(Seat *seat)
{
    if (NOTNULL(seat))
    {
        Passenger__destroy(seat->passenger);
        seat->passenger = NULL;
    }
}

void
Seat__destroy(Seat *seat)
{
    // Seat__free(seat);
    free(seat);
}