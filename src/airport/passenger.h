//
// Created by msa on 21/11/2019.
//

#ifndef ASSIGNMENT2_PASSENGER_H
#define ASSIGNMENT2_PASSENGER_H

#include <airport/flight.h>
#include <airport/seat.h>

struct passenger_t
{
    char *name;
    Flight *flight;
    enum travel_class class;
    enum priority priority;
    Seat *seat;
};

typedef struct passenger_t Passenger;

Passenger *
Passenger__create(char *name, Flight *flight, enum travel_class class, enum priority priority);

void
Passenger__init(Passenger *passenger, char *name, Flight *flight, enum travel_class class, enum priority priority);

void
Passenger__free(Passenger *passenger);

void
Passenger__destroy(Passenger *passenger);

#endif //ASSIGNMENT2_PASSENGER_H
