//
// Created by msa on 21/11/2019.
//

#include <airport/airport.h>

#include <lib/utils/memutils.h>
#include <lib/collections/queue.h>
#include <string.h>
#include <lib/utils/strutils.h>
#include <lib/utils/langutils.h>
#include <airport/flight.h>
#include <airport/passenger.h>

static bool
flight_code_predicate(void *data, void *value);

static bool
passenger_name_predicate(void *data, void *value);

void
Airport__init(Airport *airport)
{
    airport->flight_list = LinkedList__create((void (*)(void *)) &Flight__destroy);
    airport->passenger_list = LinkedList__create((void (*)(void *)) &Passenger__destroy);
}

Airport *
Airport__create()
{
    Airport *airport = ALLOC(1, Airport);
    Airport__init(airport);
    return airport;
}

void
Airport__destroy(Airport *airport)
{
    Airport__free(airport);
    free(airport);
}

void
Airport__free(Airport *airport)
{
    LinkedList__destroy(airport->flight_list);
    LinkedList__destroy(airport->passenger_list);
}

static bool
flight_code_predicate(void *data, void *value)
{
    return EQUALS(((Flight *) data)->flight_code, ((char *) value));
}

static bool
passenger_name_predicate(void *data, void *value)
{
    return EQUALS(((Passenger *) data)->name, ((char *) value));
}
/**
 * Returns the the Flight by using a predicate function that will compare flight code with each Flight's flight code in the list.
 *
 * @param airport The Airport to be searched.
 * @param flight_code The alphanumeric flight code that will be used to find the Flight.
 * @return The Flight with the supplied flight code if found, otherwise returns NULL.
 */
Flight *
Airport__get_flight(Airport *airport, char *flight_code)
{
    return (Flight *) LinkedList__get_element_by(airport->flight_list, &flight_code_predicate, flight_code);
}

/**
 * Returns the the Passenger by using a predicate function that will compare passenger name with each passenger's name in the list.
 *
 * @param airport The Airport to be searched.
 * @param flight_code The passenger name that will be used to find the Passenger.
 * @return The Passenger with the supplied name if found, otherwise returns NULL.
 */
Passenger *
Airport__get_passenger(Airport *airport, char *name)
{
    return (Passenger *) LinkedList__get_element_by(airport->passenger_list, &passenger_name_predicate, name);
}

void
Airport__schedule_flight(Airport *airport, Flight *flight)
{
    LinkedList__push_back(airport->flight_list, flight);
}
