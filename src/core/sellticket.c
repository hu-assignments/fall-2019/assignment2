//
// Created by msa on 21/11/2019.
//

#include <core/sellticket.h>
#include <lib/utils/strutils.h>
#include <lib/utils/memutils.h>
#include <stdio.h>
#include <lib/collections/priority_queue.h>
#include <airport/passenger.h>

void
execute_command(Airport *airport, char *command)
{
    if (strlen(command) > 0)
    {
        char *command_cpy = ALLOC(strlen(command) + 1, char);
        strcpy(command_cpy, command);

        unsigned int length;
        char **splt = split(command_cpy, " ", &length);
        char *base_command = splt[0];

        if (EQUALS(base_command, "addseat"))
        {
            if (length == 4)
                execute_addseat_command(airport, splt);
            else
                throw_exception();
        }
        else if (EQUALS(base_command, "enqueue"))
        {
            if (length == 4 || length == 5)
                execute_enqueue_command(airport, splt, length);
            else
                throw_exception();
        }
        else if (EQUALS(base_command, "sell"))
        {
            if (length == 2)
                execute_sell_command(airport, splt);
            else
                throw_exception();
        }
        else if (EQUALS(base_command, "close"))
        {
            if (length == 2)
                execute_close_command(airport, splt);
            else
                throw_exception();
        }
        else if (EQUALS(base_command, "report"))
        {
            if (length == 2)
                execute_report_command(airport, splt);
            else
                throw_exception();
        }
        else if (EQUALS(base_command, "info"))
        {
            if (length == 2)
                execute_info_command(airport, splt);
            else
                throw_exception();
        }
        else
            throw_exception();

        free(command_cpy);
        free(splt);
    }
}

void
execute_addseat_command(Airport *airport, char **arg_list)
{
    char *flight_code = arg_list[1];
    char *class_name = arg_list[2];
    char *str_count = arg_list[3];
    if (!is_number(str_count)) THROW_EXCEPTION()

    long count = strtol(str_count, NULL, 10);
    if (count < 0) THROW_EXCEPTION()

    Flight *flight = Airport__get_flight(airport, flight_code);

    if (flight == NULL)
    {
        flight = Flight__create(flight_code);
        Airport__schedule_flight(airport, flight);
    }

    enum travel_class class;
    if (EQUALS(class_name, "standard"))
        class = STANDARD;
    else if (EQUALS(class_name, "economy"))
        class = ECONOMY;
    else if (EQUALS(class_name, "business"))
        class = BUSINESS;
    else THROW_EXCEPTION()

    Flight__add_seat(flight, count, class);

    printf("addseats %s %d %d %d\n", flight->flight_code,
           Flight__get_number_of_seats(flight, BUSINESS),
           Flight__get_number_of_seats(flight, ECONOMY),
           Flight__get_number_of_seats(flight, STANDARD));
}

void
execute_enqueue_command(Airport *airport, char **arg_list, unsigned int len)
{
    char *flight_code = arg_list[1];
    char *class_name = arg_list[2];
    char *passenger_name = arg_list[3];
    char *priority_str = NULL;

    if (len == 5)
        priority_str = arg_list[4];
    else if (len != 4) THROW_EXCEPTION()

    Flight *flight = Airport__get_flight(airport, flight_code);

    if (flight == NULL || !flight->is_open) THROW_EXCEPTION()
    if (Airport__get_passenger(airport, passenger_name) != NULL) THROW_EXCEPTION()

    enum travel_class class;
    if (EQUALS(class_name, "standard"))
        class = STANDARD;
    else if (EQUALS(class_name, "economy"))
        class = ECONOMY;
    else if (EQUALS(class_name, "business"))
        class = BUSINESS;
    else THROW_EXCEPTION()

    enum priority p;
    if (priority_str == NULL)
        p = NONE;
    else if (EQUALS(priority_str, "diplomat"))
        p = DIPLOMAT;
    else if (EQUALS(priority_str, "veteran"))
        p = VETERAN;
    else THROW_EXCEPTION()

    if (p == DIPLOMAT && class != BUSINESS) THROW_EXCEPTION()
    if (p == VETERAN && class != ECONOMY) THROW_EXCEPTION()

    if (class == STANDARD && (p == VETERAN || p == DIPLOMAT))
        p = NONE;
    if (class == ECONOMY && (p == DIPLOMAT))
        p = NONE;
    if (class == BUSINESS && (p == VETERAN))
        p = NONE;
    int priority_new = (int) (p * class);

    Passenger *passenger = Passenger__create(passenger_name, flight, class, p);

    LinkedList__push_back(airport->passenger_list, passenger);

    PriorityQueue__enqueue(flight->ticket_queue, passenger, priority_new);

    printf("queue %s %s %s %d\n", flight->flight_code, passenger->name, class_name,
           Flight__get_number_of_people_in_ticket_queue(flight, class));
}

void
execute_sell_command(Airport *airport, char **arg_list)
{
    char *flight_code = arg_list[1];

    Flight *flight = Airport__get_flight(airport, flight_code);

    if (flight == NULL || !flight->is_open) THROW_EXCEPTION()

    Flight__sell_all(flight);
}

void
execute_close_command(Airport *airport, char **arg_list)
{
    char *flight_code = arg_list[1];

    Flight *flight = Airport__get_flight(airport, flight_code);

    if (flight == NULL || !flight->is_open) THROW_EXCEPTION()

    Flight__close(flight);
}

void
execute_report_command(Airport *airport, char **arg_list)
{
    char *flight_code = arg_list[1];

    Flight *flight = Airport__get_flight(airport, flight_code);

    if (flight == NULL) THROW_EXCEPTION()

    Flight__print_report(flight);
}

void
execute_info_command(Airport *airport, char **arg_list)
{
    char *passenger_name = arg_list[1];

    Passenger *passenger = Airport__get_passenger(airport, passenger_name);

    if (passenger == NULL) THROW_EXCEPTION()

    char *str_seat_class;
    char *str_class;

    enum travel_class seat_class;
    enum travel_class class = passenger->class;

    if (passenger->seat == NULL)
        str_seat_class = "none";
    else
    {
        seat_class = passenger->seat->class;
        switch (seat_class)
        {
            case BUSINESS:
                str_seat_class = "business";
                break;
            case ECONOMY:
                str_seat_class = "economy";
                break;
            case STANDARD:
                str_seat_class = "standard";
                break;
            default: THROW_EXCEPTION()
        }
    }

    switch (class)
    {
        case BUSINESS:
            str_class = "business";
            break;
        case ECONOMY:
            str_class = "economy";
            break;
        case STANDARD:
            str_class = "standard";
            break;
        default: THROW_EXCEPTION()
    }

    printf("info %s %s %s %s\n", passenger->name, passenger->flight->flight_code, str_class, str_seat_class);
}

void
throw_exception()
{
    printf("error\n");
}