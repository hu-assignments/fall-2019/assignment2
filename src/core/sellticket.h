//
// Created by msa on 21/11/2019.
//

#ifndef ASSIGNMENT2_SELLTICKET_H
#define ASSIGNMENT2_SELLTICKET_H

#include <lib/collections/queue.h>
#include <airport/airport.h>

#define THROW_EXCEPTION(free_exprs) { throw_exception(); free_exprs return; }

void
execute_command(Airport *airport, char *command);

void
execute_addseat_command(Airport *airport, char **arg_list);

void
execute_enqueue_command(Airport *airport, char **arg_list, unsigned int len);

void
execute_sell_command(Airport *airport, char **arg_list);

void
execute_close_command(Airport *airport, char **arg_list);

void
execute_report_command(Airport *airport, char **arg_list);

void
execute_info_command(Airport *airport, char **arg_list);

void
throw_exception();

#endif //ASSIGNMENT2_SELLTICKET_H
