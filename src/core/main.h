//
// Created by msa on 21/11/2019.
//

#ifndef ASSIGNMENT2_MAIN_H
#define ASSIGNMENT2_MAIN_H

#include <airport/airport.h>

void
begin_execution(Airport *airport, char *raw_commands);

#endif //ASSIGNMENT2_MAIN_H
