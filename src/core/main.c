#include <stdio.h>
#include <lib/utils/memutils.h>
#include <string.h>
#include <airport/airport.h>
#include <lib/utils/fileutils.h>
#include <core/main.h>
#include <airport/seat.h>
#include <lib/utils/strutils.h>
#include "sellticket.h"

int
main(int argc, char *argv[])
{
    char *input_filename = argv[1];
    char *output_filename = argv[2];

    // Re-routing the standard output to "output.txt".
    freopen(output_filename, "w", stdout);

    Airport *airport = Airport__create();

    char *file_content = unify_lines(read_file(input_filename));
    begin_execution(airport, file_content);

    Airport__destroy(airport);
    free(file_content);

    return 0;
}

void
begin_execution(Airport *airport, char *raw_commands)
{
    char *line = raw_commands;
    while (line != NULL)
    {
        char *next_line = strchr(line, '\n');
        if (next_line != NULL)
            *next_line = '\0';

        char *command = ALLOC(strlen(line) + 1, char);
        strcpy(command, line);
        command[strlen(line)] = '\0';

        execute_command(airport, command);

        free(command);

        line = (next_line != NULL) ? (next_line + 1) : NULL;
    }
}