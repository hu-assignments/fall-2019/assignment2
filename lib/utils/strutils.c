//
// Created by MSA on 26/10/2019.
//

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

#include <lib/utils/strutils.h>
#include <lib/utils/memutils.h>
#include <lib/utils/memutils.h>

unsigned int
count_substring(char *str, char *sub)
{
    unsigned int count = 0;
    size_t sub_len = strlen(sub);

    while (*str != '\0')
    {
        if (strncmp(str++, sub, sub_len) != false)
            continue;
        str += sub_len - 1;
        count++;
    }

    return count;
}

/**
 * Splits the supplied string with the specified delimiter, and returns a dynamically allocated
 * string array.
 *
 * @param str The string to be split. Note that it will be modified!
 * @param delimiter The delimiter that will be used to split the string.
 * @param length The integer pointer that will modify the integer it points to, by writing the length of the returned string array.
 * @return An array of strings.
 */
char **
split(char *str, char *delimiter, unsigned int *length)
{
    if (length != NULL)
        *length = 0;

    unsigned int i = 0;
    char **array = ALLOC((count_substring(str, delimiter) + 1), char *);
    char *p = strtok(str, delimiter);

    while (p != NULL)
    {
        array[i++] = p;

        if (length != NULL)
            (*length)++;

        p = strtok(NULL, delimiter);
    }

    return array;
}

char *
trim(char *str)
{
    /* For leading whitespaces. */
    unsigned long i;
    while (str[0] == ' ')
    {
        unsigned long length = strlen(str);
        for (i = 0; i < length; i++)
        {
            str[i] = str[i + 1];
        }
    }

    /* For trailing whitespaces. */
    while (str[strlen(str) - 1] == ' ')
    {
        str[strlen(str) - 1] = '\0';
    }

    return str;
}

char *
replace_str(char *str, char *sub, char *replacement)
{
    unsigned long len_sub = strlen(sub);
    unsigned long len_rep = strlen(replacement);

    if (len_rep > len_sub)
    {
        // Enlarge the string.
        unsigned long new_len = strlen(str) + (len_rep - len_sub) * count_substring(str, sub);
        str = REALLOC(str, new_len + 1, char);
    }

    char *ptr = strstr(str, sub);
    while (ptr != NULL)
    {
        char *str_tail = ptr + len_sub;
        char *temp_tail = ALLOC(strlen(str_tail) + 1, char);
        strcpy(temp_tail, str_tail);
        strcpy(ptr, replacement);
        strcat(str, temp_tail);
        free(temp_tail);
        ptr = strstr(ptr + len_rep, sub);
    }

    if (len_rep < len_sub)
    {
        // Shrink the string.
        str = REALLOC(str, strlen(str) + 1, char);
    }

    return str;
}

char *
unify_lines(char *content)
{
    if (content == NULL)
        return NULL;

    if (CONTAINS(content, "\r\n"))
    {
        content = replace_str(content, "\r\n", "\n");
        return unify_lines(content);
    }
    else if (CONTAINS(content, "\n\n"))
    {
        content = replace_str(content, "\n\n", "\n");
        return unify_lines(content);
    }
    else
        return content;
}

bool
is_number(char *str)
{
    unsigned int length, i;
    length = strlen(str);
    for (i = 0; i < length; i++)
        if (!isdigit(str[i]))
        {
            return false;
        }
    return true;
}