/**
 * Created on 22/11/2019.
 * @author M. Samil Atesoglu
 */

#include <lib/utils/memutils.h>

void
safe_free(void **ptr)
{
    if (ptr != NULL && *ptr != NULL)
    {
        free(*ptr);
        *ptr = NULL;
    }
}