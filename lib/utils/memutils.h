/**
 * Created on 13/10/2019.
 * @author M. Samil Atesoglu
 */

#ifndef MEM_UTILS_H
#define MEM_UTILS_H

#include <stdlib.h>

#define ALLOC(count, type) (type *) calloc(count, sizeof(type))
#define REALLOC(ptr, new_size, type) (type *) realloc(ptr, (new_size) * sizeof(type))

#define GENERIC_PTR_ADDRESS(ptr) ((void **) &(ptr))
#define SAFE_FREE(ptr) safe_free(GENERIC_PTR_ADDRESS(ptr))
#define REF(val) &(val)

void
safe_free(void **ptr);

#endif //MEM_UTILS_H
