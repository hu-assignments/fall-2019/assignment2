//
// Created by MSA on 14/10/2019.
//

#ifndef MATH_UTILS_H
#define MATH_UTILS_H

#include <math.h>

#define MATH_ADD '+'
#define MATH_SUBTRACT '-'
#define MATH_MULTIPLY '*'

/**
 * Returns the minimum value in the integer array.
 *
 * @param array The integer array to be searched.
 * @param c The size of the array.
 * @return The minimum value of the array.
 */
int
min(const int *array, unsigned int c);

/**
 * Returns the maximum value in the integer array.
 *
 * @param array The integer array to be searched.
 * @param c The size of the array.
 * @return The maximum value of the array.
 */
int
max(const int *array, unsigned int c);

#endif //MATH_UTILS_H
