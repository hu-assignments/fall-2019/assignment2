//
// Created by MSA on 26/10/2019.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <lib/utils/fileutils.h>
#include <lib/utils/memutils.h>

/**
 * Reads the file from the specified path. If cannot open the file, exits the program.
 *
 * @param path The path of the file.
 * @return The contents of the file.
 */
char *
read_file(const char *path)
{
    FILE *f = fopen(path, "rb");
    if (f == NULL)
    {
        char *msg = ALLOC(30 + strlen(path), char);
        sprintf(msg, "Error while opening the file %s", path);
        perror(msg);
        free(msg);
        exit(EXIT_FAILURE);
    }

    fseek(f, 0, SEEK_END);
    long int fsize = ftell(f);
    fseek(f, 0, SEEK_SET); /* To reset seek. */

    char *string = ALLOC(fsize + 1, char);
    fread(string, fsize, 1, f);
    fclose(f);

    string[fsize] = '\0';

    return string;
}

/**
 * Reads the file from the specified path. If it cannot read the file, returns NULL.
 *
 * @param path The path of the file to be read.
 * @return The content of the file. If the file cannot be opened, returns NULL.
 */
char *
nullable_read_file(const char *path)
{
    FILE *f = fopen(path, "rb");
    if (f == NULL)
    {
        return NULL;
    }

    fseek(f, 0, SEEK_END);
    unsigned int fsize = ftell(f);
    fseek(f, 0, SEEK_SET); /* To reset seek. */

    char *string = ALLOC(fsize + 1, char);
    fread(string, fsize, 1, f);
    fclose(f);

    string[fsize] = '\0';

    return string;
}

char *
nullable_read_from_fd(int fd)
{
    FILE *f = fdopen(fd, "rb");
    if (f == NULL)
    {
        return NULL;
    }

    fseek(f, 0, SEEK_END);
    unsigned int fsize = ftell(f);
    fseek(f, 0, SEEK_SET); /* To reset seek. */

    char *string = ALLOC(fsize + 1, char);
    fread(string, fsize, 1, f);
    fclose(f);

    string[fsize] = '\0';

    return string;
}