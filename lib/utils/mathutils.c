//
// Created by msa on 13/11/2019.
//

#include <lib/utils/mathutils.h>

/**
 * Returns the minimum value in the integer array by iterating through each element.
 *
 * @param array The integer array to be searched.
 * @param c The size of the array.
 * @return The minimum value of the array.
 */
int
min(const int *array, unsigned int c)
{
    unsigned int i;
    int min = 0;
    for (i = 0; i < c; i++)
    {
        if (i == 0 || min > array[i])
            min = array[i];
    }

    return min;
}

/**
 * Returns the maximum value in the integer array by iterating through each element.
 *
 * @param array The integer array to be searched.
 * @param c The size of the array.
 * @return The maximum value of the array.
 */
int
max(const int *array, unsigned int c)
{
    unsigned int i;
    int max = 0;
    for (i = 0; i < c; i++)
    {
        if (i == 0 || max < array[i])
            max = array[i];
    }

    return max;
}
