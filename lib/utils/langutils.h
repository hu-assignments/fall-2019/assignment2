/**
 * Created on 26/11/2019.
 * @author M. Samil Atesoglu
 */

#ifndef LANGUTILS_H
#define LANGUTILS_H

#define LAMBDA(fdef) ({fdef f;})

#define lambda(l_ret_type, l_arguments, l_body)         \
  ({                                                    \
    l_ret_type l_anonymous_functions_name l_arguments   \
      l_body                                            \
    &l_anonymous_functions_name;                        \
  })

#define NOTNULL(ptr) (ptr != NULL)

#endif // LANGUTILS_H
