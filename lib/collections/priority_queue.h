
/**
 * Created on 19/11/2019.
 * @author M. Samil Atesoglu
 */

#ifndef LIB_COLLECTIONS_PRIORITY_QUEUE_H
#define LIB_COLLECTIONS_PRIORITY_QUEUE_H

#include <lib/collections/queue.h>

struct priority_item_t
{
    int priority;
    void *val;

};

typedef struct priority_item_t PriorityItem;

void
PriorityQueue__enqueue(Queue *priority_queue, void *data, int priority);

void *
PriorityQueue__dequeue(Queue *queue);

void
PriorityQueue__free(Queue *queue);

void
PriorityQueue__destroy(Queue *queue);

#endif // LIB_COLLECTIONS_PRIORITY_QUEUE_H
