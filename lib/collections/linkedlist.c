/**
 * Created on 27/10/2019.
 * @author M. Samil Atesoglu
 */

#include <lib/collections/linkedlist.h>
#include <lib/utils/memutils.h>

LinkedList *
LinkedList__create(void (*free_fun)(void *))
{
    LinkedList *list = ALLOC(1, LinkedList);
    LinkedList__init(list, free_fun);
    return list;
}

void
LinkedList__init(LinkedList *list, void (*free_fun)(void *))
{
    list->head = NULL;
    list->free_fun = free_fun;
}

/**
 * Allocates a Node element and sets it's data to the supplied pointer.
 * Then pushes that Node element to the beginning of the linked list.
 *
 * @param list The LinkedList to be modified.
 * @param data New data to be pushed.
 */
void
LinkedList__push(LinkedList *list, void *data)
{
    if (list->head == NULL)
    {
        Node *head_node = ALLOC(1, Node);
        head_node->data = data;
        head_node->next = NULL;
        list->head = head_node;
    }
    else
    {
        Node **head = &(list->head);
        Node *new_node = ALLOC(1, Node);
        new_node->data = data;
        new_node->next = *head;
        *head = new_node;
    }
}

/**
 * Allocates a Node element and sets it's data to the supplied pointer.
 * Then pushes that Node element to the end of the linked list using "LinkedList__get_tail" method.
 *
 * @param list The LinkedList to be modified.
 * @param data New data to be inserted at the end of the linked list.
 */
void
LinkedList__push_back(LinkedList *list, void *data)
{
    if (list->head == NULL)
    {
        Node *head_node = ALLOC(1, Node);
        head_node->data = data;
        head_node->next = NULL;
        list->head = head_node;
    }
    else
    {
        Node *tail_node = LinkedList__get_tail(list);
        Node *new_node = ALLOC(1, Node);
        new_node->data = data;
        new_node->next = NULL;
        tail_node->next = new_node;
    }
}

/**
 * Traverses the list from its head to the tail, increasing the counter in each iteration.
 *
 * @param list The LinkedList that we want the size of.
 * @return The length of the list.
 */
unsigned int
LinkedList__length(LinkedList *list)
{
    Node *current = list->head;

    unsigned int length = 0;
    while (current != NULL)
    {
        length++;
        current = current->next;
    }

    return length;
}

void *
LinkedList__pop(LinkedList *list)
{
    Node **head = &(list->head);

    void *return_value;
    Node *next_node = NULL;

    if (*head == NULL)
    {
        return NULL;
    }

    next_node = (*head)->next;
    return_value = (*head)->data;
    free(*head);
    *head = next_node;

    return return_value;
}

/**
 * Iterates through every node of the list, and using the lists free function,
 * calls the containing object's free function to free that element. However, it does not free
 * the list itself. If the LinkedList is dynamically allocated, it must be freed after this function is called.
 *
 * @param list The LinkedList whose nodes will be freed.
 */
void
LinkedList__free(LinkedList *list)
{
    Node *current;
    while (list->head != NULL)
    {
        current = list->head;
        list->head = current->next;

        if (list->free_fun)
        {
            list->free_fun(current->data);
        }

        free(current);
    }
}

/**
 * Iteratively traverses the supplied list with executing the supplied function in each iteration.
 *
 * @param list The LinkedList to be operated on.
 * @param function The function to be executed for every node.
 */
void
LinkedList__for_each(LinkedList *list, void (*function)(void *))
{
    Node *current = list->head;
    while (current != NULL)
    {
        function(current->data);
        current = current->next;
    }
}

/**
 * Recursively iterates through the list with supplied function and the next node,
 * so that it can execute the supplied function in reverse order for every node of the list.
 *
 * @param current A node of the LinkedList, which would be the next node with each recursive call.
 * @param function A pointer that points to the function to be executed for each node.
 */
void
LinkedList__for_each_reversed(Node *current, void (*function)(void *))
{
    if (current == NULL)
        return;
    LinkedList__for_each_reversed(current->next, function);
    function(current->data);
}

/**
 * Iteratively traverses the list's nodes until the last node has been reached, and returns that node.
 *
 * @param list The LinkedList that we want the last node of.
 * @return The last Node of the list.
 */
Node *
LinkedList__get_tail(LinkedList *list)
{
    Node *current = list->head;
    while (current->next != NULL)
    {
        current = current->next;
    }

    return current;
}

/**
 * Iterates through the list until the specified index has been reached, and returns that Node's value.
 *
 * @param list The LinkedList that we want the i'th index of.
 * @param index The index that we want the value of.
 * @return The data that resides in the supplied index of the list.
 */
void *
LinkedList__get_by_index(LinkedList *list, unsigned int index)
{
    index = LinkedList__length(list) - index - 1;
    Node *current = list->head;

    unsigned int current_index = 0;
    while (current != NULL)
    {
        if (current_index == index)
            return current->data;
        current_index++;
        current = current->next;
    }
    return NULL;
}

bool
LinkedList__is_empty(LinkedList *list)
{
    return LinkedList__length(list) == 0;
}

/**
 * Free's all the "Node"s in the dynamically allocated LinkedList and their contents by calling free function
 * of the containing object, then frees itself.
 * This function should only be called if the supplied LinkedList is dynamically allocated.
 *
 * @param list Dynamically allocated LinkedList.
 */
void
LinkedList__destroy(LinkedList *list)
{
    LinkedList__free(list);
    free(list);
}

/**
 * Returns the element from the list based on the supplied predicate function.
 *
 * @param list The linked list.
 * @param predicate_fun The function to decide whether an element should be returned or not.
 * @param value The value that will be used for the predicate function.
 * @return The element that satisfies the predicate function.
 */
void *
LinkedList__get_element_by(LinkedList *list, bool (*predicate_fun)(void *, void *), void *value)
{
    Node *current = list->head;
    while (current != NULL)
    {
        if (predicate_fun(current->data, value))
            return current->data;
        current = current->next;
    }

    return NULL;
}

