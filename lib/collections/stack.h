//
// Created by msa on 21/11/2019.
//

#ifndef LIB_COLLECTIONS_STACK_H
#define LIB_COLLECTIONS_STACK_H

#include <lib/collections/linkedlist.h>

struct stack_t
{
    LinkedList *list;
};

typedef struct stack_t Stack;

Stack *
Stack__create(void (*free_fun)(void *));

void
Stack__init(Stack *stack, void (*free_fun)(void *));

void
Stack__push(Stack *stack, void *data);

void *
Stack__pop(Stack *stack);

void *
Stack__peek(Stack *stack);

unsigned int
Stack__size(Stack *stack);

bool
Stack__is_empty(Stack *stack);

void
Stack__free(Stack *stack);

void
Stack__destroy(Stack *stack);

#endif // LIB_COLLECTIONS_STACK_H
