//
// Created by msa on 21/11/2019.
//

#include <stdbool.h>
#include <lib/collections/stack.h>
#include <lib/utils/memutils.h>
#include <lib/utils/langutils.h>

Stack *
Stack__create(void (*free_fun)(void *))
{
    Stack *stack = ALLOC(1, Stack);
    Stack__init(stack, free_fun);
    return stack;
}

void
Stack__init(Stack *stack, void (*free_fun)(void *))
{
    LinkedList *list = ALLOC(1, LinkedList);
    LinkedList__init(list, free_fun);

    stack->list = list;
    stack->list->head = NULL;
    stack->list->free_fun = free_fun;
}

void
Stack__push(Stack *stack, void *data)
{
    LinkedList__push(stack->list, data);
}

void *
Stack__pop(Stack *stack)
{
    return LinkedList__pop(stack->list);
}

void *
Stack__peek(Stack *stack)
{
    return LinkedList__get_by_index(stack->list, 0);
}

unsigned int
Stack__size(Stack *stack)
{
    return LinkedList__length(stack->list);
}

bool
Stack__is_empty(Stack *stack)
{
    return LinkedList__length(stack->list) == 0;
}

void
Stack__free(Stack *stack)
{
    if (NOTNULL(stack))
    {
        LinkedList__free(stack->list);
        free(stack->list);
    }
}

void
Stack__destroy(Stack *stack)
{
    Stack__free(stack);
    free(stack);
}