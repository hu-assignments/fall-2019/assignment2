/**
 * Created on 19/11/2019.
 * @author M. Samil Atesoglu
 */

#include <lib/collections/queue.h>
#include <lib/utils/memutils.h>
#include <lib/utils/langutils.h>

Queue *
Queue__create(void (*free_fun)(void *))
{
    Queue *queue = ALLOC(1, Queue);
    Queue__init(queue, free_fun);
    return queue;
}

void
Queue__init(Queue *queue, void (*free_fun)(void *))
{
    queue->list = LinkedList__create(free_fun);
    queue->list->head = NULL;
    queue->list->free_fun = free_fun;
}

void
Queue__enqueue(Queue *queue, void *data)
{
    LinkedList__push_back(queue->list, data);
}

void *
Queue__dequeue(Queue *queue)
{
    return LinkedList__pop(queue->list);
}

unsigned int
Queue__size(Queue *queue)
{
    return LinkedList__length(queue->list);
}

bool
Queue__is_empty(Queue *queue)
{
    return Queue__size(queue) == 0;
}

void
Queue__free(Queue *queue)
{
    if (NOTNULL(queue))
    {
        LinkedList__free(queue->list);
        free(queue->list);
    }
}

void
Queue__destroy(Queue *queue)
{
    Queue__free(queue);
    free(queue);
}