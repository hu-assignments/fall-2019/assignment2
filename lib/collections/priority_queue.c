/**
 * Created on 19/11/2019.
 * @author M. Samil Atesoglu
 */

#include <lib/collections/priority_queue.h>
#include <lib/collections/queue.h>
#include <lib/utils/memutils.h>
#include <lib/utils/langutils.h>

void
PriorityQueue__enqueue_priority_item(Queue *priority_queue, PriorityItem *item);

/**
 * Inserts an element with specified priority to the priority queue.
 *
 * @param priority_queue The Queue that the element will be inserted.
 * @param data The new element to be inserted.
 * @param priority The priority of the element. Higher the value, higher the value.
 */
void
PriorityQueue__enqueue(Queue *priority_queue, void *data, int priority)
{
    PriorityItem *item = ALLOC(1, PriorityItem);
    item->val = data;
    item->priority = priority;
    PriorityQueue__enqueue_priority_item(priority_queue, item);
}

/**
 * Inserts a new PriorityItem to the by iterating through the elements of the queue,
 * until finding a place where the priority of the next item is smaller than itself.
 *
 * @param priority_queue The Queue that we want to insert new element to.
 * @param item The PriorityItem to be inserted.
 */
void
PriorityQueue__enqueue_priority_item(Queue *priority_queue, PriorityItem *item)
{
    if (priority_queue->list->head == NULL)
        Queue__enqueue(priority_queue, item);
    else
    {
        Node *current = priority_queue->list->head;
        Node *new_node = ALLOC(1, Node);

        if (((PriorityItem *) current->data)->priority < item->priority)
        {
            Node **head = &(priority_queue->list->head);
            new_node->data = item;
            new_node->next = *head;
            *head = new_node;
        }
        else
        {
            while (current->next != NULL && ((PriorityItem *) current->next->data)->priority >= item->priority)
                current = current->next;

            new_node->next = current->next;
            new_node->data = item;
            current->next = new_node;
        }
    }
}

void *
PriorityQueue__dequeue(Queue *queue)
{
    PriorityItem *item = Queue__dequeue(queue);
    void *val = item->val;
    free(item);
    return val;
}

void
PriorityQueue__free(Queue *queue)
{
    if (NOTNULL(queue))
    {
        Node *current;
        while (queue->list->head != NULL)
        {
            current = queue->list->head;
            queue->list->head = current->next;

            if (queue->list->free_fun)
            {
                queue->list->free_fun(((PriorityItem *) current->data)->val);
            }

            free((PriorityItem *) current->data);
            free(current);
        }

        free(queue->list);
    }
}

void
PriorityQueue__destroy(Queue *queue)
{
    PriorityQueue__free(queue);
    free(queue);
}