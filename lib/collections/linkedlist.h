/**
 * Created on 27/10/2019.
 * @author M. Samil Atesoglu
 */

#ifndef LIB_COLLECTIONS_LINKEDLIST_H
#define LIB_COLLECTIONS_LINKEDLIST_H

#include <stddef.h>
#include <stdbool.h>

#define LLIST__GET(list, index, type) ((type) LinkedList__get_by_index(list, index))

struct node_t
{
    void *data;
    struct node_t *next;
};

typedef struct node_t Node;

struct linked_list_t
{
    Node *head;
    void (*free_fun)(void *);
};

typedef struct linked_list_t LinkedList;

/**
 * Dynamically allocates and initializes a new LinkedList, and returns a pointer to that list.
 *
 * @param free_fun The free function that will be used when freeing the LinkedList.
 * It should be the containing object's free method.
 * It will be called for each node in the list when the list is about to be freed using "LinkedList__free" method.
 * @return The newly allocated and initialized LinkedList's pointer.
 */
LinkedList *
LinkedList__create(void (*free_fun)(void *));

/**
 * Pushes an element to the start of the LinkedList.
 *
 * @param list The LinkedList to be modified.
 * @param data New element to be pushed to the head of the list.
 */
void
LinkedList__push(LinkedList *list, void *data);

/**
 * Pushes an element to the end of the LinkedList.
 *
 * @param list The LinkedList to be modified.
 * @param data New element to be inserted at the end of the list.
 */
void
LinkedList__push_back(LinkedList *list, void *data);

/**
 * Removes and returns the value of the first node of the LinkedList.
 *
 * @param list The LinkedList that we want to remove the first node of.
 * @return The value of the removed first node of the list.
 */
void *
LinkedList__pop(LinkedList *list);

/**
 * Returns the number of nodes in the LinkedList.
 *
 * @param list The LinkedList that we want the number of nodes of.
 * @return The number of nodes in the list.
 */
unsigned int
LinkedList__length(LinkedList *list);

/**
 * Frees all the nodes and their values using the free function inside the LinkedList.
 * Note that it does not free the list itself, so if the list is dynamically allocated,
 * it is the caller's responsibility to free the list itself, or call "LinkedList__destroy" method instead.
 *
 * @param list The LinkedList whose nodes will be freed.
 */
void
LinkedList__free(LinkedList *list);

/**
 * Free's all the "Node"s in the dynamically allocated LinkedList, then frees itself.
 * This function should only be called if the supplied LinkedList is dynamically allocated.
 *
 * @param list Dynamically allocated LinkedList.
 */
void
LinkedList__destroy(LinkedList *list);

/**
 * Returns the value of the Node at the supplied index.
 *
 * @param list The LinkedList.
 * @param index The index.
 * @return The value of the Node at the specified index.
 */
void *
LinkedList__get_by_index(LinkedList *list, unsigned int index);

/**
 * Initializes a supplied LinkedList. The list may or may not be dynamically allocated.
 *
 * @param list A LinkedList instance to be initialized.
 * @param free_fun A free function to free the data of a Node in the list.
 */
void
LinkedList__init(LinkedList *list, void (*free_fun)(void *));

/**
 * Returns the last node in the LinkedList.
 *
 * @param list The LinkedList that we want the last node of.
 * @return A Node struct that is the last node of the list.
 */
Node *
LinkedList__get_tail(LinkedList *list);

/**
 * Checks whether the list is empty or not.
 *
 * @param list The LinkedList to be checked.
 * @return A boolean that's true if the list is empty, false otherwise.
 */
bool
LinkedList__is_empty(LinkedList *list);

/**
 * For every element in the list, executes the supplied function.
 *
 * @param list The LinkedList to be operated on.
 * @param function The function to be executed for every node.
 */
void
LinkedList__for_each(LinkedList *list, void (*function)(void *));

/**
 * For every element in the list, starting with the last element and ending with the head node,
 * calls the supplied function.
 *
 * @param current A node of the LinkedList, generally the head node.
 * @param function A pointer that points to the function to be executed for each node.
 */
void
LinkedList__for_each_reversed(Node *current, void (*function)(void *));

/**
 * Returns the element from the list based on the supplied predicate function.
 *
 * @param list The linked list.
 * @param predicate_fun The function to decide whether an element should be returned or not.
 * @param value The value that will be used for the predicate function.
 * @return The element that satisfies the predicate function.
 */
void *
LinkedList__get_element_by(LinkedList *list, bool (*predicate_fun)(void *, void *), void *value);

#endif // LIB_COLLECTIONS_LINKEDLIST_H
