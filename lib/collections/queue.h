/**
 * Created on 19/11/2019.
 * @author M. Samil Atesoglu
 */

#ifndef LIB_COLLECTIONS_QUEUE_H
#define LIB_COLLECTIONS_QUEUE_H

#include <lib/collections/linkedlist.h>

struct queue_t
{
    LinkedList *list;
};

typedef struct queue_t Queue;

Queue *
Queue__create(void (*free_fun)(void *));

void
Queue__init(Queue *queue, void (*free_fun)(void *));

void
Queue__enqueue(Queue *queue, void *data);

void *
Queue__dequeue(Queue *queue);

unsigned int
Queue__size(Queue *queue);

bool
Queue__is_empty(Queue *queue);

void
Queue__free(Queue *queue);

void
Queue__destroy(Queue *queue);

#endif // LIB_COLLECTIONS_QUEUE_H
